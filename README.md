[![pipeline status](https://gitlab.com/yakshaving.art/dockerfiles/testcafe/badges/master/pipeline.svg)](https://gitlab.com/yakshaving.art/dockerfiles/testcafe/-/commits/master)
# testcafe

Image with testcafe (for now) for testing muchos pages.

This image is multiarch, see os/architectures we build in .gitlab-ci.yml
