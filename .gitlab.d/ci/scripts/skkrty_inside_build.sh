#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is script that tests security _from inside the image_ so that it is multi-arch
# NOTE: since this runs from inside the image in Dockerfile's RUN step, take care to not pollute the image!
set -EeufCo pipefail
IFS=$'\t\n'

# safenet: bail out if we're (presumably) not inside the docker build process
if [[ "unset" == "${TARGETARCH:-unset}" ]]; then
	>&2 echo "Environment variable 'TARGETARCH' is unset: are we inside docker build? Failing."
	exit 42
fi
if [[ "unset" == "${TARGETOS:-unset}" ]]; then
	>&2 echo "Environment variable 'TARGETOS' is unset: are we inside docker build? Failing."
	exit 42
fi

# safenet: check that we map the trivy binary from the builder image
if ! command -v "trivy" >/dev/null 2>&1; then
	>&2 echo "Error! Unable to find the 'trivy' command, can't run skkrty checks from within the image. Did you mount it properly?"
	exit 42
fi

# Since the docker images are multi-arch, and docker builds them in parallel,
# we can have N cases: goss binary is of $runner-arch and image is Nth arch.
# Thus, we only run tests (for now) when those match
apk --no-cache add file
_trivy_arch="$(file -Lb "$(which trivy)" | awk -F "," '{print $2}')"
_image_arch="$(file -Lb "$(which bash)" | awk -F "," '{print $2}')"
apk --no-cache del file
if [[ "${_trivy_arch}" != "${_image_arch}" ]]; then
	>&2 echo "Architecture mismatch, skipping tests: TARGETARCH is '${TARGETARCH}', trivy/runner arch is '${_trivy_arch}', image arch is '${_image_arch}'"
	exit 0
fi

# trivy defaults
export TRIVY_QUIET="${TRIVY_QUIET:-true}"
export TRIVY_TIMEOUT="10m"

# TODO: caching? For now, simply delete it to not pollute the image
trap "rm -rf /tmp/trivy" EXIT

trivy --cache-dir /tmp/trivy image \
	--download-db-only \
	--no-progress

# Check the rootfs
trivy --cache-dir /tmp/trivy rootfs \
	--no-progress \
	--exit-code 1 \
	--severity CRITICAL \
	/

# Check the filesystem (TODO: is this dupe?)
trivy --cache-dir /tmp/trivy filesystem \
	--no-progress \
	--exit-code 1 \
	--severity CRITICAL \
	/

# If no critical issues, output all
trivy --cache-dir /tmp/trivy rootfs \
	--no-progress \
	--exit-code 0 \
	/
